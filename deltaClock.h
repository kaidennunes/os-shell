//
// Created by Kaiden on 2/12/2019.
//

#ifndef SHELL_DELTACLOCK_H
#define SHELL_DELTACLOCK_H

#include <setjmp.h>
#include "os345.h"

extern Semaphore *dcChange;

typedef struct {
    int time;
    Semaphore *sem;
} ClockUnit;

typedef struct {
    int currentLength;
    int maxLength;
    int front;
    Semaphore *mutex;
    ClockUnit *clockUnits;
} DeltaClock;

int insertDeltaClock(DeltaClock *deltaClock, int time, Semaphore *sem);

void decrementDeltaClock(DeltaClock *deltaClock);

DeltaClock createDeltaClock(int clockSize);

int destroyDeltaClock(DeltaClock *deltaClock);

#endif //SHELL_DELTACLOCK_H
