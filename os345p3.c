// os345p3.c - Jurassic Park
// ***********************************************************************
// **   DISCLAMER ** DISCLAMER ** DISCLAMER ** DISCLAMER ** DISCLAMER   **
// **                                                                   **
// ** The code given here is the basis for the CS345 projects.          **
// ** It comes "as is" and "unwarranted."  As such, when you use part   **
// ** or all of the code, it becomes "yours" and you are responsible to **
// ** understand any algorithm or method presented.  Likewise, any      **
// ** errors or problems become your responsibility to fix.             **
// **                                                                   **
// ** NOTES:                                                            **
// ** -Comments beginning with "// ??" may require some implementation. **
// ** -Tab stops are set at every 3 spaces.                             **
// ** -The function API's in "OS345.h" should not be altered.           **
// **                                                                   **
// **   DISCLAMER ** DISCLAMER ** DISCLAMER ** DISCLAMER ** DISCLAMER   **
// ***********************************************************************
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <setjmp.h>
#include <time.h>
#include <assert.h>
#include "os345.h"
#include "os345park.h"
#include "deltaClock.h"

// ***********************************************************************
// project 3 variables
extern TCB tcb[];
int timeTaskID;
int carToPassengerId;
int carToDriverId;

// Jurassic Park
extern JPARK myPark;
extern Semaphore *parkMutex;                        // protect park access
extern Semaphore *fillSeat[NUM_CARS];            // (signal) seat ready to fill
extern Semaphore *seatFilled[NUM_CARS];        // (wait) passenger seated
extern Semaphore *rideOver[NUM_CARS];            // (signal) ride over


Semaphore *dcChange;

Semaphore *visitorArriveTimer;
Semaphore *parkVisitors;
Semaphore *museumVisitors;
Semaphore *giftShopVisitors;
Semaphore *tickets;

Semaphore *getPassenger;
Semaphore *seatTaken;
Semaphore *passengerSeated;
Semaphore *passengerRide;
Semaphore *passengerRideSaved;
Semaphore *savePassengerRide;
Semaphore *needDriverMutex;
Semaphore *needDriver;
Semaphore *wakeupDriver;
Semaphore *driverReady;
Semaphore *carReady;
Semaphore *rideDone[NUM_CARS];
Semaphore *leftRide[NUM_CARS];
Semaphore *driverDone;
Semaphore *needTicket;
Semaphore *ticketReady;
Semaphore *buyTicket;
Semaphore *ticketBought;

DeltaClock deltaClock;
Semaphore *events[DELTA_CLOCK_SIZE];

extern Semaphore *tics10thsec;                // 1/10 second semaphore

// ***********************************************************************
// project 3 functions and tasks
int P3_visitors(int, char **);

int P3_cars(int, char **);

int P3_drivers(int, char **);

int dcMonitorTask(int argc, char *argv[]);

int timeTask(int argc, char *argv[]);

void printDeltaClock(void);

// ***********************************************************************
// ***********************************************************************
// project3 command
int P3_project3(int argc, char *argv[]) {
    char buf[32];
    SWAP;
    char *newArgv[2];
    SWAP;
    // start park
    sprintf(buf, "jurassicPark");
    SWAP;
    newArgv[0] = buf;
    SWAP;
    createTask(buf,                // task name
               jurassicTask,                // task
               MED_PRIORITY,                // task priority
               1,                                // task count
               newArgv);                    // task argument
    SWAP;
    // wait for park to get initialized...
    while (!parkMutex) SWAP;
    printf("\nStart Jurassic Park...");
    SWAP;
    createTask("deltaClock",                // task name
               P3_dc,                // task
               VERY_HIGH_PRIORITY,                // task priority
               1,                                // task count
               newArgv);                    // task argument
    SWAP;
    visitorArriveTimer = createSemaphore("visitorArriveTimer", BINARY, 0);
    SWAP;
    tickets = createSemaphore("tickets", COUNTING, MAX_TICKETS);
    SWAP;
    parkVisitors = createSemaphore("parkVisitors", COUNTING, MAX_IN_PARK);
    SWAP;
    museumVisitors = createSemaphore("museumVisitors", COUNTING, MAX_IN_MUSEUM);
    SWAP;
    giftShopVisitors = createSemaphore("giftShopVisitors", COUNTING, MAX_IN_GIFTSHOP);
    SWAP;
    getPassenger = createSemaphore("getPassenger", BINARY, 0);
    SWAP;
    seatTaken = createSemaphore("seatTaken", BINARY, 0);
    SWAP;
    passengerSeated = createSemaphore("passengerSeated", BINARY, 0);
    SWAP;
    savePassengerRide = createSemaphore("savePassengerRide", BINARY, 0);
    SWAP
    passengerRideSaved = createSemaphore("passengerRideSaved", BINARY, 0);
    SWAP;
    passengerRide = createSemaphore("passengerRide", BINARY, 1);
    SWAP;
    needDriverMutex = createSemaphore("needDriverMutex", COUNTING, 1);
    SWAP;
    needDriver = createSemaphore("needDriver", BINARY, 0);
    SWAP;
    wakeupDriver = createSemaphore("wakeupDriver", BINARY, 0);
    SWAP;
    driverReady = createSemaphore("driverReady", BINARY, 0);
    SWAP;
    carReady = createSemaphore("carReady", BINARY, 0);
    SWAP;
    needTicket = createSemaphore("needTicket", BINARY, 0);
    SWAP;
    ticketReady = createSemaphore("ticketReady", BINARY, 0);
    SWAP;
    buyTicket = createSemaphore("buyTicket", BINARY, 0);
    SWAP;
    ticketBought = createSemaphore("buyTicket", BINARY, 0);
    SWAP;

    int i;
    SWAP;
    for (i = 0; i < NUM_CARS; i++) {
        SWAP;
        sprintf(buf, "rideDone%d", i);
        SWAP;
        rideDone[i] = createSemaphore(buf, COUNTING, 0);
        SWAP;
        sprintf(buf, "leftRide%d", i);
        SWAP;
        leftRide[i] = createSemaphore(buf, COUNTING, 0);
        SWAP;
    }
    driverDone = createSemaphore("driverDone", BINARY, 0);
    SWAP;

    createTask("p3_visitors",                // task name
               P3_visitors,                // task
               MED_PRIORITY,                // task priority
               1,                                // task count
               newArgv);                    // task argument
    SWAP;
    createTask("p3_cars",                // task name
               P3_cars,                // task
               MED_PRIORITY,                // task priority
               1,                                // task count
               newArgv);                    // task argument
    SWAP;
    createTask("p3_drivers",                // task name
               P3_drivers,                // task
               MED_PRIORITY,                // task priority
               1,                                // task count
               newArgv);                    // task argument
    SWAP;
    return 0;
} // end project3

// ***********************************************************************
// ***********************************************************************
// visitor command
int P3_visitor(int argc, char *argv[]) {
    int visitorID = atoi(argv[0]);
    SWAP;
    char stringNumber[32];
    SWAP;
    itoa(visitorID, stringNumber, 10);
    SWAP;
    char buf[32];
    SWAP;
    sprintf(buf, "visitorWaitTimer%d", visitorID);
    SWAP;
    Semaphore *visitorWaitTimer = createSemaphore(buf, BINARY, 0);
    SWAP;
    // Get random wait period
    int waitingPeriod = rand() % 70;
    SWAP;
    // Add the waiting time to the delta clock
    insertDeltaClock(&deltaClock, waitingPeriod, visitorWaitTimer);
    SWAP;
    SEM_WAIT(visitorWaitTimer);
    SWAP;

    SWAP;
    SEM_WAIT(parkMutex);
    SWAP;
    myPark.numInPark++;
    SWAP;
    myPark.numOutsidePark--;
    SWAP;
    myPark.numInTicketLine++;
    SWAP;
    SEM_SIGNAL(parkMutex);
    SWAP;
    waitingPeriod = rand() % 30;
    SWAP;
    // Add the waiting time to the delta clock
    insertDeltaClock(&deltaClock, waitingPeriod, visitorWaitTimer);
    SWAP;
    SEM_WAIT(visitorWaitTimer);
    SWAP;

    SEM_WAIT(needDriverMutex);
    SWAP;

    // signal need ticket (signal, put hand up)
    SEM_SIGNAL(needTicket);
    SWAP;

    // wakeup driver (signal)
    SEM_SIGNAL(wakeupDriver);
    SWAP;

    // wait ticket available (signal)
    SEM_WAIT(ticketReady);
    SWAP;

    // buy ticket (signal)
    SEM_SIGNAL(buyTicket);
    SWAP;

    SEM_WAIT(ticketBought);
    SWAP;

    // release driver (mutex)
    SEM_SIGNAL(needDriverMutex);
    SWAP;

    // Get out of ticket line and into museum line
    waitingPeriod = rand() % 30;
    SWAP;

    SEM_WAIT(parkMutex);
    SWAP;
    myPark.numInTicketLine--;
    SWAP;
    myPark.numInMuseumLine++;
    SWAP;

    SEM_SIGNAL(parkMutex);
    SWAP;
    // Add the waiting time to the delta clock
    insertDeltaClock(&deltaClock, waitingPeriod, visitorWaitTimer);
    SWAP;

    SEM_WAIT(visitorWaitTimer);
    SWAP;
    // Get into the museum
    SEM_WAIT(museumVisitors);
    SWAP;
    // Get out of line and into the museum
    SEM_WAIT(parkMutex);
    SWAP;
    myPark.numInMuseumLine--;
    SWAP;
    myPark.numInMuseum++;
    SWAP;
    SEM_SIGNAL(parkMutex);
    SWAP;

    // Spend some time in the museum
    waitingPeriod = rand() % 70;
    SWAP;
    // Add the waiting time to the delta clock
    insertDeltaClock(&deltaClock, waitingPeriod, visitorWaitTimer);
    SWAP;

    SEM_WAIT(visitorWaitTimer);
    SWAP;
    // Get out of the museum and into car line
    SEM_SIGNAL(museumVisitors);
    SWAP;

    SEM_WAIT(parkMutex);
    SWAP;
    myPark.numInMuseum--;
    SWAP;
    myPark.numInCarLine++;
    SWAP;
    SEM_SIGNAL(parkMutex);
    SWAP;

    // Wait for a car to have room for passengers
    SEM_WAIT(getPassenger);
    SWAP;
    // Spend some time getting into the seat
    waitingPeriod = rand() % 10;
    SWAP;
    // Add the waiting time to the delta clock
    insertDeltaClock(&deltaClock, waitingPeriod, visitorWaitTimer);
    SWAP;

    SEM_WAIT(visitorWaitTimer);
    SWAP;

    // Signal that the visitor took the seat
    SEM_SIGNAL(seatTaken);
    SWAP;

    SEM_WAIT(parkMutex);
    SWAP;
    myPark.numInCarLine--;
    SWAP;
    myPark.numInCars++;
    SWAP;
    SEM_SIGNAL(parkMutex);
    SWAP;


    // Save the car ID
    SEM_WAIT(savePassengerRide);
    SWAP;
    int passengerCarID = carToPassengerId;
    SWAP;

    SEM_SIGNAL(passengerRideSaved);
    SWAP;

    // Release visitor ticket
    SEM_WAIT(parkMutex);
    SWAP;
    myPark.numTicketsAvailable++;
    SWAP;
    SEM_SIGNAL(parkMutex);
    SWAP;

    SEM_SIGNAL(tickets);
    SWAP;

    // Wait for the car ride to be over
    SEM_WAIT(rideDone[passengerCarID]);
    SWAP;

    SEM_SIGNAL(leftRide[passengerCarID]);

    // Get out of the car and into the gift shop line
    SWAP;
    SEM_WAIT(parkMutex);
    SWAP;
    myPark.numInCars--;
    SWAP;
    myPark.numInGiftLine++;
    SWAP;
    SEM_SIGNAL(parkMutex);
    SWAP;
    // Add the waiting time to the delta clock
    waitingPeriod = rand() % 30;
    SWAP;
    insertDeltaClock(&deltaClock, waitingPeriod, visitorWaitTimer);
    SWAP;
    SEM_WAIT(visitorWaitTimer);
    // Get into the gift shop
    SWAP;
    SEM_WAIT(giftShopVisitors);
    // Get out of line and into the gift shop
    SWAP;

    SEM_WAIT(parkMutex);
    SWAP;
    myPark.numInGiftLine--;
    SWAP;
    myPark.numInGiftShop++;
    SWAP;
    SEM_SIGNAL(parkMutex);
    SWAP;

    // Spend some time in the gift shop
    waitingPeriod = rand() % 70;
    SWAP;
    // Add the waiting time to the delta clock
    insertDeltaClock(&deltaClock, waitingPeriod, visitorWaitTimer);
    SWAP;

    SEM_WAIT(visitorWaitTimer);
    SWAP;

    // Get out of the gift shop and out of the park
    SEM_SIGNAL(giftShopVisitors);
    SWAP;
    SEM_SIGNAL(parkVisitors);
    SWAP;

    SEM_WAIT(parkMutex);
    SWAP;
    myPark.numInGiftShop--;
    SWAP;
    myPark.numInPark--;
    SWAP;
    myPark.numExitedPark++;
    SWAP;
    SEM_SIGNAL(parkMutex);
    SWAP;

    return 0;
} // end P3_visitor

// ***********************************************************************
// ***********************************************************************
// visitors command
int P3_visitors(int argc, char *argv[]) {
    int i;
    SWAP;
    for (i = 0; i < NUM_VISITORS; i++) {
        SWAP;
        // Get random wait period
        int waitingPeriod = rand() % 40;
        SWAP;
        // Add the waiting time to the delta clock
        insertDeltaClock(&deltaClock, waitingPeriod, visitorArriveTimer);
        SWAP;
        SEM_WAIT(visitorArriveTimer);
        SWAP;
        SEM_WAIT(parkMutex);
        SWAP;
        myPark.numOutsidePark++;
        SWAP;
        SEM_SIGNAL(parkMutex);
        SWAP;
        SEM_WAIT(parkVisitors);
        SWAP;
        char stringNumber[32];
        SWAP;
        itoa(i, stringNumber, 10);
        SWAP;
        char *newArgv[2];
        SWAP;
        char buf[32];
        SWAP;
        sprintf(buf, "p3_visitor%d", i);
        SWAP;
        newArgv[0] = stringNumber;
        SWAP;
        createTask(buf,                // task name
                   P3_visitor,                // task
                   MED_PRIORITY,                // task priority
                   1,                                // task count
                   newArgv);                    // task argument
        SWAP;
    }
    SWAP;
    return 0;
} // end P3_visitors

// ***********************************************************************
// ***********************************************************************
// car command
int P3_car(int argc, char *argv[]) {
    int carID = atoi(argv[0]);
    SWAP;
    Semaphore *currentDriverDone;
    SWAP;
    int i;
    SWAP;
    while (1) {
        SWAP;
        // For each car, do 3 times, one for each seat
        for (i = 0; i < NUM_SEATS; i++) {
            SWAP;
            SEM_WAIT(fillSeat[carID]);
            SWAP; // wait for available seat

            SEM_SIGNAL(getPassenger);
            SWAP; // signal for visitor

            SEM_WAIT(seatTaken);
            SWAP; // wait for visitor to reply

            // ... save passenger ride over semaphore ...
            SEM_WAIT(passengerRide);
            SWAP;
            carToPassengerId = carID;

            SEM_SIGNAL(savePassengerRide);
            SWAP;

            SEM_WAIT(passengerRideSaved);
            SWAP;


            SEM_SIGNAL(passengerRide);
            SWAP;

            SEM_SIGNAL(passengerSeated);
            SWAP // signal visitor in seat
            // if last passenger, get driver
            if (i == NUM_SEATS - 1) {
                SWAP;
                SEM_WAIT(needDriverMutex);
                SWAP;

                SEM_SIGNAL(needDriver);
                SWAP;
                // wakeup attendant
                SEM_SIGNAL(wakeupDriver);
                SWAP;

                SEM_WAIT(driverReady);
                SWAP;
                // ... save driver ride over semaphore ...
                currentDriverDone = driverDone;
                SWAP;
                carToDriverId = carID;
                SWAP;
                SEM_SIGNAL(carReady);
                SWAP;
                SEM_WAIT(driverReady);
                SWAP;
                // got driver (mutex)
                SEM_SIGNAL(needDriverMutex);
                SWAP;
            }
            SEM_SIGNAL(seatFilled[carID]);
            SWAP; // signal next seat ready
        }
        SWAP;
        SEM_WAIT(rideOver[carID]);
        SWAP; // wait for ride over

        SEM_SIGNAL(currentDriverDone);
        SWAP;

        // Tell each passenger that their ride is over
        int j;
        SWAP;
        for (j = 0; j < NUM_SEATS; j++) {
            SWAP;
            SEM_SIGNAL(rideDone[carID]);
            SWAP;
        }

        // Wait for all the passengers to get off
        for (j = 0; j < NUM_SEATS; j++) {
            SWAP;
            SEM_WAIT(leftRide[carID]);
            SWAP;
        }
        SWAP;
    }
    return 0;
} // end P3_car

// ***********************************************************************
// ***********************************************************************
// cars command
int P3_cars(int argc, char *argv[]) {
    int i;
    SWAP;
    for (i = 0; i < NUM_CARS; i++) {
        SWAP;
        char stringNumber[32];
        SWAP;
        itoa(i, stringNumber, 10);
        SWAP;
        char *newArgv[2];
        SWAP;
        char buf[32];
        SWAP;
        sprintf(buf, "p3_car%d", i);
        SWAP;
        newArgv[0] = stringNumber;
        SWAP;
        createTask(buf,                // task name
                   P3_car,                // task
                   MED_PRIORITY,                // task priority
                   1,                                // task count
                   newArgv);                    // task argument
        SWAP;
    }
    SWAP;
    return 0;
} // end P3_cars

// ***********************************************************************
// ***********************************************************************
// driver command
int P3_driver(int argc, char *argv[]) {
    char buf[32];
    SWAP;
    int driverID = atoi(argv[0]);
    SWAP
    sprintf(buf, "P3_driver%d", driverID);
    SWAP;
    Semaphore *driverDoneSemaphore = createSemaphore(buf, BINARY, 0);
    SWAP;
    sprintf(buf, "driverWaitTimer%d", driverID);
    SWAP;
    Semaphore *visitorWaitTimer = createSemaphore(buf, BINARY, 0);
    SWAP;
    while (1) {
        SWAP;
        SEM_WAIT(wakeupDriver);
        SWAP;    // goto sleep
        // See if driver is needed
        int neededDriver = SEM_TRYLOCK(needDriver);
        SWAP;
        if (neededDriver) {                    // yes
            SWAP;
            driverDone = driverDoneSemaphore;
            SWAP;
            SEM_SIGNAL(driverReady);
            SWAP;
            SEM_WAIT(carReady);
            SWAP;
            int carId = carToDriverId;
            SWAP
            SEM_SIGNAL(driverReady);
            SWAP;    // wait for car ready to go
            SEM_WAIT(parkMutex);
            SWAP;
            myPark.drivers[driverID] = carId + 1;
            SWAP;
            SEM_SIGNAL(parkMutex);
            SWAP;
            SEM_WAIT(driverDoneSemaphore);
            SWAP;    // wait for car ready to go
            SEM_WAIT(parkMutex);
            SWAP;
            myPark.drivers[driverID] = 0;
            SWAP;
            SEM_SIGNAL(parkMutex);
            continue;
        }
        int neededTicket = SEM_TRYLOCK(needTicket);
        if (neededTicket)            // See if someone needs a ticket
        {                    // yes
            SWAP;    // wait for car ready to go
            SEM_WAIT(parkMutex);
            SWAP;
            myPark.drivers[driverID] = -1;
            SWAP;
            SEM_SIGNAL(parkMutex);
            SWAP;
            int waitingPeriod = rand() % 20;
            SWAP;
            // Add the waiting time to the delta clock
            insertDeltaClock(&deltaClock, waitingPeriod, visitorWaitTimer);
            SWAP;
            SEM_WAIT(visitorWaitTimer);
            SWAP;
            SEM_WAIT(tickets);
            SWAP;    // wait for ticket (counting)
            SEM_WAIT(parkMutex);
            SWAP;
            myPark.numTicketsAvailable--;
            SWAP;
            SEM_SIGNAL(parkMutex);
            SWAP;
            SEM_SIGNAL(ticketReady);
            SWAP;    // print a ticket (binary)
            SEM_WAIT(buyTicket);
            SWAP;
            SEM_WAIT(parkMutex);
            SWAP;
            myPark.drivers[driverID] = 0;
            SWAP;
            SEM_SIGNAL(parkMutex);
            SWAP;
            SEM_SIGNAL(ticketBought);
            SWAP;
        }
        SWAP;
        // don’t bother me!
    }
    SWAP;
    return 0;
} // end P3_driver

// ***********************************************************************
// ***********************************************************************
// drivers command
int P3_drivers(int argc, char *argv[]) {
    int i;
    SWAP;
    for (i = 0; i < NUM_DRIVERS; i++) {
        SWAP;
        char stringNumber[32];
        SWAP;
        itoa(i, stringNumber, 10);
        SWAP;
        char *newArgv[2];
        SWAP;
        char buf[32];
        SWAP;
        sprintf(buf, "p3_driver%d", i);
        SWAP;
        newArgv[0] = stringNumber;
        SWAP;
        createTask(buf,                // task name
                   P3_driver,                // task
                   MED_PRIORITY,                // task priority
                   1,                                // task count
                   newArgv);                    // task argument
        SWAP;
    }

    return 0;
} // end P3_drivers

// ***********************************************************************
// ***********************************************************************
// ***********************************************************************
// ***********************************************************************
// ***********************************************************************
// ***********************************************************************
// delta clock command
int P3_dc_tick(int argc, char *argv[]) {
    while (1) {
        SWAP;
        SEM_WAIT(tics10thsec);
        SWAP;
        decrementDeltaClock(&deltaClock);
        SWAP;
    }
    SWAP;
    return 0;
} // end CL3_dc

// ***********************************************************************
// ***********************************************************************
// ***********************************************************************
// ***********************************************************************
// ***********************************************************************
// ***********************************************************************
// delta clock command
int P3_dc(int argc, char *argv[]) {
    printf("\nDelta Clock");
    SWAP;
    if (deltaClock.clockUnits == NULL) {
        SWAP;
        deltaClock = createDeltaClock(DELTA_CLOCK_SIZE);
        SWAP;
        createTask("deltaClockTick",                // task name
                   P3_dc_tick,                // task
                   VERY_HIGH_PRIORITY,                // task priority
                   1,                                // task count
                   argv);
        SWAP;
    }
    printDeltaClock();
    SWAP;
    return 0;
} // end CL3_dc


// ***********************************************************************
// display all pending events in the delta clock list
void printDeltaClock(void) {
    int i;
    SWAP;
    SEM_WAIT(deltaClock.mutex);
    SWAP;
    for (i = 0; i < deltaClock.currentLength; i++) {
        SWAP;
        int index = (deltaClock.front + 1 + i) % (deltaClock.maxLength);
        SWAP;
        printf("\n%4d%4d  %-20s", i, deltaClock.clockUnits[index].time, deltaClock.clockUnits[index].sem->name);
        SWAP;
    }
    SWAP;
    SEM_SIGNAL(deltaClock.mutex);
    SWAP;
    return;
}


// ***********************************************************************
// test delta clock
int P3_tdc(int argc, char *argv[]) {
    P3_dc(argc, argv);
    SWAP;
    createTask("DC Test",            // task name
               dcMonitorTask,        // task
               10,                    // task priority
               argc,                    // task arguments
               argv);
    SWAP;
    timeTaskID = createTask("Time",        // task name
                            timeTask,    // task
                            10,            // task priority
                            argc,            // task arguments
                            argv);
    SWAP;
    return 0;
} // end P3_tdc



// ***********************************************************************
// monitor the delta clock task
int dcMonitorTask(int argc, char *argv[]) {
    int i, flg;
    SWAP;
    char buf[32];
    SWAP;
    // create some test times for event[0-9]
    int ttime[10] = {
            300, 90, 50, 170, 340, 300, 50, 300, 40, 110};
    SWAP;
    for (i = 0; i < 10; i++) {
        SWAP;
        sprintf(buf, "event[%d]", i);
        SWAP;
        events[i] = createSemaphore(buf, BINARY, 0);
        SWAP;
        insertDeltaClock(&deltaClock, ttime[i], events[i]);
        SWAP;
    }
    SWAP;
    printDeltaClock();
    SWAP;

    while (deltaClock.currentLength > 0) {
        SWAP;
        SEM_WAIT(dcChange);
        SWAP;
        flg = 0;
        SWAP;
        for (i = 0; i < 10; i++) {
            SWAP;
            if (events[i]->state == 1) {
                SWAP;
                printf("\n  event[%d] signaled", i);
                SWAP;
                events[i]->state = 0;
                SWAP;
                flg = 1;
                SWAP;
            }
            SWAP;
        }
        SWAP;
        if (flg) {
            SWAP;
            printDeltaClock();
        }
        SWAP;
    }
    SWAP;
    printf("\nNo more events in Delta Clock");
    SWAP;
    // kill dcMonitorTask
    tcb[timeTaskID].state = S_EXIT;
    SWAP;
    return 0;
} // end dcMonitorTask


extern Semaphore *tics1sec;

// ********************************************************************************************
// display time every tics1sec
int timeTask(int argc, char *argv[]) {
    char svtime[64];                        // ascii current time
    SWAP;
    while (1) {
        SWAP;
        SEM_WAIT(tics1sec)
        SWAP;
        printf("\nTime = %s", myTime(svtime));
        SWAP;
    }
    SWAP;
    return 0;
} // end timeTask

