//
// Created by Kaiden on 2/12/2019.
//

#include <malloc.h>
#include <stdio.h>
#include "deltaClock.h"

int insertDeltaClock(DeltaClock *deltaClock, int time, Semaphore *sem) {
    /* Lock the buffer */
    SEM_WAIT(deltaClock->mutex);
    SWAP;
    if (deltaClock->currentLength == deltaClock->maxLength) {
        SWAP;
        SEM_SIGNAL(deltaClock->mutex);
        SWAP;
        return 0;
    }
    SWAP;
    int deltaTime = time;
    SWAP;
    int i;
    SWAP;
    for (i = 0; i < deltaClock->currentLength; i++) {
        SWAP;
        int index = (deltaClock->front + 1 + i) % (deltaClock->maxLength);
        SWAP;
        int currentTimeSlice = deltaClock->clockUnits[index].time;
        SWAP;
        if (currentTimeSlice > deltaTime) {
            SWAP;
            deltaClock->clockUnits[index].time -= deltaTime;
            SWAP;
            break;
        }
        SWAP;
        deltaTime -= currentTimeSlice;
        SWAP;
    }
    SWAP;
    int previousValue = deltaTime;
    SWAP;
    Semaphore *previousSem = sem;
    SWAP;
    for (; i < deltaClock->currentLength; i++) {
        SWAP;
        int index = (deltaClock->front + 1 + i) % (deltaClock->maxLength);
        SWAP;
        int newPreviousValue = deltaClock->clockUnits[index].time;
        SWAP;
        Semaphore *newPreviousSem = deltaClock->clockUnits[index].sem;
        SWAP;

        deltaClock->clockUnits[index].time = previousValue;
        SWAP;
        deltaClock->clockUnits[index].sem = previousSem;
        SWAP;

        previousValue = newPreviousValue;
        SWAP;
        previousSem = newPreviousSem;
        SWAP;
    }
    SWAP;

    int index = (deltaClock->front + 1 + i) % (deltaClock->maxLength);
    SWAP;
    deltaClock->clockUnits[index].time = previousValue;
    SWAP;
    deltaClock->clockUnits[index].sem = previousSem;
    SWAP;
    deltaClock->currentLength++;
    SWAP;

    /* Unlock the buffer */
    SEM_SIGNAL(deltaClock->mutex);
    SWAP;
    return 1;
}

void decrementDeltaClock(DeltaClock *deltaClock) {
    /* Lock the buffer */
    SEM_WAIT(deltaClock->mutex);
    SWAP;

    if (deltaClock->currentLength == 0) {
        SWAP;
        SEM_SIGNAL(deltaClock->mutex);
        SWAP;
        return;
    }
    SWAP;
    /* Decrement the first item */
    int new_front = (deltaClock->front + 1) % (deltaClock->maxLength);
    SWAP;
    deltaClock->clockUnits[new_front].time--;
    SWAP;

    // If the time is 0 or less, than signal the semaphore and remove the item from the queue
    while (deltaClock->currentLength > 0 && deltaClock->clockUnits[new_front].time <= 0) {
        SWAP;
        SEM_SIGNAL(deltaClock->clockUnits[new_front].sem);
        SWAP;
        deltaClock->front = new_front;
        SWAP;
        new_front = (deltaClock->front + 1) % (deltaClock->maxLength);
        SWAP;
        deltaClock->currentLength--;
        SWAP;
    }
    SWAP;
    /* Unlock the buffer */
    SEM_SIGNAL(deltaClock->mutex);
    SWAP;
    SEM_SIGNAL(dcChange);
    SWAP;
}

DeltaClock createDeltaClock(int clockSize) {
    dcChange = createSemaphore("dcChange", BINARY, 1);
    SWAP;
    DeltaClock deltaClock;
    SWAP;
    deltaClock.mutex = createSemaphore("dcMutex", COUNTING, 1);
    SWAP;
    deltaClock.maxLength = clockSize;
    SWAP;
    deltaClock.currentLength = 0;
    SWAP;
    deltaClock.clockUnits = malloc(clockSize * sizeof(ClockUnit));
    SWAP;
    return deltaClock;
}

int destroyDeltaClock(DeltaClock *deltaClock) {
    /* Lock the buffer */
    SEM_WAIT(deltaClock->mutex);
    SWAP;
    if (deltaClock != NULL && deltaClock->clockUnits != NULL) {
        SWAP;
        free(deltaClock->clockUnits);
        SWAP;
    }
    SWAP;
    /* Unlock the buffer */
    SEM_SIGNAL(deltaClock->mutex);
    SWAP;
    return 1;
}