// os345mmu.c - LC-3 Memory Management Unit	03/05/2017
//
//		03/12/2015	added PAGE_GET_SIZE to accessPage()
//
// **************************************************************************
// **   DISCLAMER ** DISCLAMER ** DISCLAMER ** DISCLAMER ** DISCLAMER   **
// **                                                                   **
// ** The code given here is the basis for the CS345 projects.          **
// ** It comes "as is" and "unwarranted."  As such, when you use part   **
// ** or all of the code, it becomes "yours" and you are responsible to **
// ** understand any algorithm or method presented.  Likewise, any      **
// ** errors or problems become your responsibility to fix.             **
// **                                                                   **
// ** NOTES:                                                            **
// ** -Comments beginning with "// ??" may require some implementation. **
// ** -Tab stops are set at every 3 spaces.                             **
// ** -The function API's in "OS345.h" should not be altered.           **
// **                                                                   **
// **   DISCLAMER ** DISCLAMER ** DISCLAMER ** DISCLAMER ** DISCLAMER   **
// ***********************************************************************

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <setjmp.h>
#include <assert.h>
#include "os345.h"
#include "os345lc3.h"

// ***********************************************************************
// mmu variables

// LC-3 memory
unsigned short int memory[LC3_MAX_MEMORY];

// statistics
int memAccess;                        // memory accesses
int memHits;                        // memory hits
int memPageFaults;                    // memory faults
int clockRPT;                        // RPT clock
int clockUPT;                        // UPT clock

int getFrame(int);

int getAvailableFrame(void);

int runClock(int notMe);

extern TCB tcb[];                    // task control block
extern int curTask;                    // current task #

int getFrame(int notMe) {
    int frame;
    frame = getAvailableFrame();
    if (frame >= 0) return frame;

    // run clock
    return runClock(notMe);
}

int runClock(int notMe) {
    int i, j;
    unsigned short int rpta, rpte1, rpte2;
    unsigned short int upta, upte1, upte2;

    // Keep running the clock until you find a frame to use
    while (1) {
        // Go through every possible root page table entry
        for (i = clockRPT; i < LC3_RPT_COUNT; i++) {
            // Get the root page table entry address
            rpta = LC3_RPT + (clockRPT * 2);

            // Get the root page table entry from memory
            rpte1 = memory[rpta];
            rpte2 = memory[rpta + 1];

            // If the root page table entry doesn't exist, or if the entry doesn't have an associated frame (user page table), then move on
            if (rpte1 == 0 || !DEFINED(rpte1)) {
                clockRPT = (clockRPT + 1) % LC3_RPT_COUNT;
                continue;
            }

            // If this root page table entry has been referenced, then mark it as not referenced and move on to the next entry
            if (REFERENCED(rpte1)) {
                memory[rpta] = CLEAR_REF(rpte1);
                clockRPT = (clockRPT + 1) % LC3_RPT_COUNT;
                continue;
            }

            // For each entry in the user page table, go through each user page table
            for (j = clockUPT; j < LC3_FRAME_SIZE / 2; j++) {
                // Get the user page table entry address
                upta = (FRAME(rpte1) << 6) + (clockUPT * 2);

                // Get the user page table entry
                upte1 = memory[upta];
                upte2 = memory[upta + 1];

                // No matter what, you will have to move the clock, so do that now
                clockUPT = (clockUPT + 1) % (LC3_FRAME_SIZE / 2);

                // If the user page table entry doesn't exist, if the frame isn't defined, or if the frame is the one we don't want to pick, then move on
                if (upte1 == 0 || !DEFINED(upte1) || FRAME(upte1) == notMe) {
                    continue;
                }

                // If the frame doesn't have the reference bit set, set the swap bit, and put the frame into swap space, then use that frame
                if (!REFERENCED(upte1)) {
                    if (PAGED(upte2) && DIRTY(upte1)) {
                        accessPage(SWAPPAGE(upte2), FRAME(upte1), PAGE_OLD_WRITE);
                    } else if (!PAGED(upte2)) {
                        int pnum = accessPage(SWAPPAGE(upte2), FRAME(upte1), PAGE_NEW_WRITE);
                        upte2 = SET_PAGED(pnum);
                    }
                    memory[upta] = 0;
                    memory[upta + 1] = upte2;
                    return FRAME(upte1);
                } else {
                    // Otherwise, unset the reference bit
                    memory[upta] = CLEAR_REF(upte1);
                }
            }

            // Increment the root page table clock
            clockRPT = (clockRPT + 1) % LC3_RPT_COUNT;

            // If we can't discard this frame (user page table), then move on
            if (FRAME(rpte1) == notMe) {
                continue;
            }

            bool canDiscardUserPageTable = TRUE;
            // If the user page table has no references, then just use that frame
            for (j = 0; j < LC3_FRAME_SIZE / 2; j++) {
                // Get the user page table entry address
                upta = (FRAME(rpte1) << 6) + (j * 2);

                // Get the user page table entry
                upte1 = memory[upta];
                upte2 = memory[upta + 1];

                // If any of the entries in the user page table are referenced, then we can't use this page table
                if (REFERENCED(upte1) || DEFINED(upte1)) {
                    canDiscardUserPageTable = FALSE;
                    break;
                }
            }

            // If we can discard the user page table, then just use its frame
            if (canDiscardUserPageTable) {
                if (PAGED(rpte2) && DIRTY(rpte1)) {
                    accessPage(SWAPPAGE(rpte2), FRAME(rpte1), PAGE_OLD_WRITE);
                } else if (!PAGED(rpte2)) {
                    int pnum = accessPage(SWAPPAGE(rpte2), FRAME(rpte1), PAGE_NEW_WRITE);
                    rpte2 = SET_PAGED(pnum);
                }
                memory[rpta] = 0;
                memory[rpta + 1] = rpte2;
                return FRAME(rpte1);
            }
        }
    }
}

// **************************************************************************
// **************************************************************************
// LC3 Memory Management Unit
// Virtual Memory Process
// **************************************************************************
//           ___________________________________Frame defined
//          / __________________________________Dirty frame
//         / / _________________________________Referenced frame
//        / / / ________________________________Pinned in memory
//       / / / /     ___________________________
//      / / / /     /                 __________frame # (0-1023) (2^10)
//     / / / /     /                 / _________page defined
//    / / / /     /                 / /       __page # (0-4096) (2^12)
//   / / / /     /                 / /       /
//  / / / /     / 	             / /       /
// F D R P - - f f|f f f f f f f f|S - - - p p p p|p p p p p p p p

unsigned short int *getMemAdr(int va, int rwFlg) {
    unsigned short int rpta, rpte1, rpte2;
    unsigned short int upta, upte1, upte2;

    // turn off virtual addressing for system RAM
    if (va < 0x3000) return &memory[va];
    rpta = tcb[curTask].RPT + RPTI(va);        // root page table address
    rpte1 = memory[rpta];                    // FDRP__ffffffffff
    rpte2 = memory[rpta + 1];                    // S___pppppppppppp
    memAccess++;
    // rpte defined
    if (DEFINED(rpte1)) {
        memHits++;
    } else {
        memPageFaults++;
        rpte1 = SET_DEFINED(getFrame(-1));
        if (PAGED(rpte2)) {
            accessPage(SWAPPAGE(rpte2), FRAME(rpte1), PAGE_READ);
        } else {
            rpte1 = SET_DIRTY(rpte1);
            rpte2 = 0;
            memset(&memory[FRAME(rpte1) << 6], 0, 64 * sizeof(memory[0]));
        }
    }

    // set rpt frame access bit
    memory[rpta] = rpte1 = SET_REF(SET_PINNED(rpte1));
    memory[rpta + 1] = rpte2;

    // user page table address
    upta = (FRAME(rpte1) << 6) + UPTI(va);
    // FDRP__ffffffffff
    upte1 = memory[upta];
    // S___pppppppppppp
    upte2 = memory[upta + 1];
    memAccess++;
    // upte defined
    if (DEFINED(upte1)) {
        memHits++;
    } else {
        memPageFaults++;
        upte1 = SET_DEFINED(getFrame(FRAME(rpte1)));
        if (PAGED(upte2)) {
            accessPage(SWAPPAGE(upte2), FRAME(upte1), PAGE_READ);
        } else {
            upte1 = SET_DIRTY(upte1);
            upte2 = 0;
        }
    }
    // set upt frame access bit
    memory[upta] = upte1 = SET_REF(upte1);
    memory[upta + 1] = upte2;
    if (rwFlg) {
        memory[rpta] = SET_DIRTY(rpte1);
        memory[upta] = SET_DIRTY(upte1);
    }
    return &memory[(FRAME(upte1) << 6) + FRAMEOFFSET(va)];
} // end getMemAdr


// **************************************************************************
// **************************************************************************
// set frames available from sf to ef
//    flg = 0 -> clear all others
//        = 1 -> just add bits
//
void setFrameTableBits(int flg, int sf, int ef) {
    int i, data;
    int adr = LC3_FBT - 1;             // index to frame bit table
    int fmask = 0x0001;              // bit mask

    // 1024 frames in LC-3 memory
    for (i = 0; i < LC3_FRAMES; i++) {
        if (fmask & 0x0001) {
            fmask = 0x8000;
            adr++;
            data = (flg) ? MEMWORD(adr) : 0;
        } else fmask = fmask >> 1;
        // allocate frame if in range
        if ((i >= sf) && (i < ef)) data = data | fmask;
        MEMWORD(adr) = data;
    }
    return;
} // end setFrameTableBits


// **************************************************************************
// get frame from frame bit table (else return -1)
int getAvailableFrame() {
    int i, data;
    int adr = LC3_FBT - 1;                // index to frame bit table
    int fmask = 0x0001;                    // bit mask

    for (i = 0; i < LC3_FRAMES; i++)        // look thru all frames
    {
        if (fmask & 0x0001) {
            fmask = 0x8000;                // move to next work
            adr++;
            data = MEMWORD(adr);
        } else fmask = fmask >> 1;        // next frame
        // deallocate frame and return frame #
        if (data & fmask) {
            MEMWORD(adr) = data & ~fmask;
            return i;
        }
    }
    return -1;
} // end getAvailableFrame



// **************************************************************************
// read/write to swap space
int accessPage(int pnum, int frame, int rwnFlg) {
    static int nextPage;                        // swap page size
    static int pageReads;                        // page reads
    static int pageWrites;                        // page writes
    static unsigned short int swapMemory[LC3_MAX_SWAP_MEMORY];

    if ((nextPage >= LC3_MAX_PAGE) || (pnum >= LC3_MAX_PAGE)) {
        printf("\nVirtual Memory Space Exceeded!  (%d)", LC3_MAX_PAGE);
        exit(-4);
    }
    switch (rwnFlg) {
        case PAGE_INIT:                            // init paging
            clockRPT = 0;                        // clear RPT clock
            clockUPT = 0;                        // clear UPT clock
            memAccess = 0;                        // memory accesses
            memHits = 0;                        // memory hits
            memPageFaults = 0;                    // memory faults
            nextPage = 0;                        // disk swap space size
            pageReads = 0;                        // disk page reads
            pageWrites = 0;                        // disk page writes
            return 0;

        case PAGE_GET_SIZE:                        // return swap size
            return nextPage;

        case PAGE_GET_READS:                    // return swap reads
            return pageReads;

        case PAGE_GET_WRITES:                    // return swap writes
            return pageWrites;

        case PAGE_GET_ADR:                        // return page address
            return (int) (&swapMemory[pnum << 6]);

        case PAGE_NEW_WRITE:                   // new write (Drops thru to write old)
            pnum = nextPage++;

        case PAGE_OLD_WRITE:                   // write
            //printf("\n    (%d) Write frame %d (memory[%04x]) to page %d", p.PID, frame, frame<<6, pnum);
            memcpy(&swapMemory[pnum << 6], &memory[frame << 6], 1 << 7);
            pageWrites++;
            return pnum;

        case PAGE_READ:                        // read
            //printf("\n    (%d) Read page %d into frame %d (memory[%04x])", p.PID, pnum, frame, frame<<6);
            memcpy(&memory[frame << 6], &swapMemory[pnum << 6], 1 << 7);
            pageReads++;
            return pnum;

        case PAGE_FREE:                   // free page
            printf("\nPAGE_FREE not implemented");
            break;
    }
    return pnum;
} // end accessPage
