#ifndef __priorityqueue_h__
#define __priorityqueue_h__

typedef struct {
    int id;
    int priority;
} priorityItem;

typedef struct {
    int currentQueueSize;
    int maxQueueSize;
    priorityItem *queue;
} priorityQueue;

priorityQueue createPriorityQueue(int queueSize);

int moveQueueItem(priorityQueue *queueToRemove, priorityQueue *queueToAdd, int item);

int moveAllQueueItems(priorityQueue *queueToRemove, priorityQueue *queueToAdd);

int destroyPriorityQueue(priorityQueue *queue);

int getHighestPriority(priorityQueue *queue);

int insertQueueItem(priorityQueue *queue, int item, int priority);

#endif
