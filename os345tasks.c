// os345tasks.c - OS create/kill task	08/08/2013
// ***********************************************************************
// **   DISCLAMER ** DISCLAMER ** DISCLAMER ** DISCLAMER ** DISCLAMER   **
// **                                                                   **
// ** The code given here is the basis for the BYU CS345 projects.      **
// ** It comes "as is" and "unwarranted."  As such, when you use part   **
// ** or all of the code, it becomes "yours" and you are responsible to **
// ** understand any algorithm or method presented.  Likewise, any      **
// ** errors or problems become your responsibility to fix.             **
// **                                                                   **
// ** NOTES:                                                            **
// ** -Comments beginning with "// ??" may require some implementation. **
// ** -Tab stops are set at every 3 spaces.                             **
// ** -The function API's in "OS345.h" should not be altered.           **
// **                                                                   **
// **   DISCLAMER ** DISCLAMER ** DISCLAMER ** DISCLAMER ** DISCLAMER   **
// ***********************************************************************

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <setjmp.h>
#include <time.h>
#include <assert.h>

#include "os345.h"
#include "os345signals.h"
#include "priorityqueue.h"
#include "os345lc3.h"


extern TCB tcb[];                            // task control block
extern int curTask;                            // current task #

extern int superMode;                        // system mode
extern Semaphore *semaphoreList;            // linked list of active semaphores
extern Semaphore *taskSems[MAX_TASKS];        // task semaphore

extern priorityQueue readyQueue;

// **********************************************************************
// **********************************************************************
// fair share time helper
void proportionTimesHelper(int taskId) {
    int tid;
    // Find out how many children this task has
    int numberOfChildrenTasks = 0;
    int numberOfActiveChildrenTasks = 0;
    for (tid = 0; tid < MAX_TASKS; tid++) {
        // If this task is the active child task of the given task, increment the child count
        if (tid != taskId && tcb[tid].name && tcb[tid].parent == taskId) {
            numberOfChildrenTasks++;
            if (tcb[tid].event == 0) {
                numberOfActiveChildrenTasks++;
            }
        }
    }

    // If there are no children tasks, then stop here
    if (numberOfChildrenTasks == 0) {
        return;
    }

    int totalAllotment = tcb[taskId].taskTime;
    int taskTimeAllotment = totalAllotment / (numberOfActiveChildrenTasks + 1);
    int parentTaskLeftOverAllotment = totalAllotment % (numberOfActiveChildrenTasks + 1);
    // Give this task its allotment plus anything left over from the division
    if (tcb[taskId].event == 0) {
        tcb[taskId].taskTime = taskTimeAllotment + parentTaskLeftOverAllotment;
    }

    // Go back through this task's children and give them their allotment, then recursively do the same for their children
    for (tid = 0; tid < MAX_TASKS; tid++) {
        // If this task is the child task of the given task, then give it an allotment and recursively go through its children
        if (tid != taskId && tcb[tid].name && tcb[tid].parent == taskId) {
            if (tcb[tid].event == 0) {
                tcb[tid].taskTime = taskTimeAllotment;
            }
            proportionTimesHelper(tid);
        }
    }
}

// **********************************************************************
// **********************************************************************
// fair share time
void proportionTimes() {
    // Give the shell the whole allotment
    tcb[0].taskTime = 1000;
    // Start with the shell
    proportionTimesHelper(0);
}

// **********************************************************************
// **********************************************************************
// create task
int createTask(char *name,                        // task name
               int (*task)(int, char **),    // task address
               int priority,                // task priority
               int argc,                    // task argument count
               char *argv[])                // task argument pointers
{
    int tid;

    // find an open tcb entry slot
    for (tid = 0; tid < MAX_TASKS; tid++) {
        if (tcb[tid].name == 0) {
            char buf[8];

            // create task semaphore
            if (taskSems[tid]) deleteSemaphore(&taskSems[tid]);
            sprintf(buf, "task%d", tid);
            taskSems[tid] = createSemaphore(buf, 0, 0);
            taskSems[tid]->taskNum = 0;    // assign to shell

            // copy task name
            tcb[tid].name = (char *) malloc(strlen(name) + 1);
            strcpy(tcb[tid].name, name);

            // set task address and other parameters
            tcb[tid].task = task;            // task address
            tcb[tid].state = S_NEW;            // NEW task state
            tcb[tid].priority = priority;    // task priority
            tcb[tid].parent = curTask;        // parent
            tcb[tid].argc = argc;            // argument count

            // Malloc new argv parameters
            tcb[tid].argv = malloc(argc * sizeof(char *));
            for (int i = 0; i < argc; i++) {
                // Malloc memory for each argv argument
                size_t argumentSize = strlen(argv[i]) + 1;
                tcb[tid].argv[i] = malloc(sizeof(char) * argumentSize);
                strncpy(tcb[tid].argv[i], argv[i], argumentSize);
            }

            tcb[tid].event = 0;                // suspend semaphore
            tcb[tid].RPT = LC3_RPT + ((tid) ? ((tid - 1) << 6) : 0);
            tcb[tid].taskTime = 0;
            tcb[tid].cdir = CDIR;            // inherit parent cDir (project 7)

            // define task signals
            createTaskSigHandlers(tid);

            // Each task must have its own stack and stack pointer.
            tcb[tid].stack = malloc(STACK_SIZE * sizeof(int));

            // Insert task into ready queue
            insertQueueItem(&readyQueue, tid, priority);

            if (tid) swapTask();                // do context switch (if not cli)

            return tid;                            // return tcb index (curTask)
        }
    }

    // tcb full!
    return -1;
} // end createTask



// **********************************************************************
// **********************************************************************
// kill task
//
//	taskId == -1 => kill all non-shell tasks
//
static void exitTask(int taskId);

int killTask(int taskId) {
    if (taskId != 0)            // don't terminate shell
    {
        if (taskId < 0)            // kill all tasks
        {
            int tid;
            for (tid = 1; tid < MAX_TASKS; tid++) {
                if (tcb[tid].name) exitTask(tid);
            }
        } else {
            // terminate individual task
            if (!tcb[taskId].name) return 1;
            exitTask(taskId);    // kill individual task
        }
    } else {
        insertQueueItem(&readyQueue, taskId, tcb[taskId].priority);
    }
    if (!superMode) SWAP;
    return 0;
} // end killTask

static void exitTask(int taskId) {
    assert("exitTaskError" && tcb[taskId].name);
    tcb[taskId].state = S_EXIT;            // EXIT task state
    return;
} // end exitTask



// **********************************************************************
// system kill task
//
int sysKillTask(int taskId) {
    Semaphore *sem = semaphoreList;
    Semaphore **semLink = &semaphoreList;

    // assert that you are not pulling the rug out from under yourself!
    assert("sysKillTask Error" && tcb[taskId].name && superMode);

    // Free up argv memory
    for (int i = 0; i < tcb[taskId].argc; i++) {
        free(tcb[taskId].argv[i]);
    }
    free(tcb[taskId].argv);

    // signal task terminated
    semSignal(taskSems[taskId]);

    // look for any semaphores created by this task
    while (sem = *semLink) {
        if (sem->taskNum == taskId) {
            // semaphore found, delete from list, release memory
            deleteSemaphore(semLink);
        } else {
            // move to next semaphore
            semLink = (Semaphore **) &sem->semLink;
        }
    }

    tcb[taskId].name = 0;            // release tcb slot
    return 0;
} // end sysKillTask
