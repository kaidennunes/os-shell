// os345interrupts.c - pollInterrupts	08/08/2013
// ***********************************************************************
// **   DISCLAMER ** DISCLAMER ** DISCLAMER ** DISCLAMER ** DISCLAMER   **
// **                                                                   **
// ** The code given here is the basis for the BYU CS345 projects.      **
// ** It comes "as is" and "unwarranted."  As such, when you use part   **
// ** or all of the code, it becomes "yours" and you are responsible to **
// ** understand any algorithm or method presented.  Likewise, any      **
// ** errors or problems become your responsibility to fix.             **
// **                                                                   **
// ** NOTES:                                                            **
// ** -Comments beginning with "// ??" may require some implementation. **
// ** -Tab stops are set at every 3 spaces.                             **
// ** -The function API's in "OS345.h" should not be altered.           **
// **                                                                   **
// **   DISCLAMER ** DISCLAMER ** DISCLAMER ** DISCLAMER ** DISCLAMER   **
// ***********************************************************************

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <setjmp.h>
#include <time.h>
#include <assert.h>

#include "os345.h"
#include "os345config.h"
#include "os345signals.h"
#include "deltaClock.h"


// **********************************************************************
//	local prototypes
//
void pollInterrupts(void);

static void keyboard_isr(void);

static void timer_isr(void);

// **********************************************************************
// **********************************************************************
// global semaphores

extern Semaphore *keyboard;                // keyboard semaphore
extern Semaphore *charReady;                // character has been entered
extern Semaphore *inBufferReady;            // input buffer ready semaphore

extern Semaphore *tics1sec;                // 1 second semaphore
extern Semaphore *tics10thsec;                // 1/10 second semaphore
extern Semaphore *tics10sec;                // 10 second semaphore

extern char inChar;                // last entered character
extern int charFlag;                // 0 => buffered input
extern int inBufIndx;                // input pointer into input buffer
extern int inBuffCurrentIndex;                // input pointer into input buffer
extern char inBuffer[INBUF_SIZE + 1];    // character input buffer

extern time_t oldTime1;                    // old 1sec time
extern clock_t myClkTime;
extern clock_t myOldClkTime;
extern clock_t myOldClkTime10;

extern DeltaClock deltaClock;

extern int pollClock;                // current clock()
extern int lastPollClock;            // last pollClock

extern int superMode;                        // system mode

extern char *getPreviousCommand();

extern char *getNextCommand();

// **********************************************************************
// **********************************************************************
// simulate asynchronous interrupts by polling events during idle loop
//
void pollInterrupts(void) {
    // check for task monopoly
    pollClock = clock();
    assert("Timeout" && ((pollClock - lastPollClock) < MAX_CYCLES));
    lastPollClock = pollClock;

    // check for keyboard interrupt
    if ((inChar = GET_CHAR) > 0) {
        keyboard_isr();
    }

    // timer interrupt
    timer_isr();

    return;
} // end pollInterrupts


// **********************************************************************
// keyboard interrupt service routine
//
static void keyboard_isr() {
    // assert system mode
    assert("keyboard_isr Error" && superMode);

    semSignal(charReady);                    // SIGNAL(charReady) (No Swap)
    if (charFlag == 0) {
        // printf("'0x%x'", 0xff & inChar);
        switch (inChar) {
            case '\r':
            case '\n': {
                inBufIndx = inBuffCurrentIndex = 0;                // EOL, signal line ready
                semSignal(inBufferReady);    // SIGNAL(inBufferReady)
                break;
            }
            case 0x18:                        // ^x
            {
                printf("\nKilling all tasks");
                inBufIndx = inBuffCurrentIndex = 0;
                inBuffer[0] = 0;
                sigSignal(0, mySIGINT);        // interrupt task 0
                semSignal(inBufferReady);    // SEM_SIGNAL(inBufferReady)
                break;
            }
            case 0x12:                        // ^r
            {
                printf("\nContinuing all tasks");
                sigSignal(-1, mySIGCONT);        // continues all tasks
                break;
            }
            case 0x17:                        // ^w
            {
                printf("\nStopping all tasks");
                sigSignal(-1, mySIGTSTP);        // stops all tasks
                break;
            }
            case 0x8:                            // Backspace
            {
                if (inBufIndx > 0) {
                    // Replace characters on the prompt
                    printf("\b \b");
                    // Remove the previous character from the in buffer
                    memmove(inBuffer + inBuffCurrentIndex - 1, inBuffer + inBuffCurrentIndex - 1 + 1,
                            strlen(inBuffer) - inBuffCurrentIndex);
                    inBuffer[--inBufIndx] = 0;
                    inBuffCurrentIndex--;

                    if (inBufIndx != inBuffCurrentIndex) {
                        int i = inBuffCurrentIndex;
                        for (i = inBuffCurrentIndex; i < inBufIndx; i++) {
                            printf("%c", inBuffer[i]);
                        }
                        printf(" ");
                        for (i = inBuffCurrentIndex; i < inBufIndx + 1; i++) {
                            printf("\b");
                        }
                    }
                }
                break;
            }
            case 0x60: {                        // up arrow
                char *previousCommand = getPreviousCommand();
                if (previousCommand != NULL) {
                    while (inBufIndx > 0) {
                        printf("\b \b");
                        inBuffer[--inBufIndx] = 0;
                    }
                    printf("%s", previousCommand);
                    size_t commandLineSize = strlen(previousCommand) + 1;
                    strncpy(inBuffer, previousCommand, commandLineSize);
                    inBufIndx = inBuffCurrentIndex = commandLineSize - 1;
                }
                break;
            }
            case 0x7e: {                        // down arrow
                char *nextCommand = getNextCommand();
                if (nextCommand != NULL) {
                    while (inBufIndx > 0) {
                        printf("\b \b");
                        inBuffer[--inBufIndx] = 0;
                    }
                    printf("%s", nextCommand);
                    size_t commandLineSize = strlen(nextCommand) + 1;
                    strncpy(inBuffer, nextCommand, commandLineSize);
                    inBufIndx = inBuffCurrentIndex = commandLineSize - 1;
                }
                break;
            }
            case 0x3C: {                        // left arrow
                // Don't allow underflows
                if (inBuffCurrentIndex == 0) {
                    break;
                }

                inBuffCurrentIndex--;
                printf("\b");
                break;
            }
            case 0x3E: {                        // right arrow
                // Don't allow overflows
                if (inBuffCurrentIndex == inBufIndx) {
                    break;
                }
                printf("%c", inBuffer[inBuffCurrentIndex]);
                inBuffCurrentIndex++;
                break;
            }

            default: {
                // Don't allow overflows
                if (inBufIndx == INBUF_SIZE) {
                    break;
                }

                if (inBufIndx != inBuffCurrentIndex) {
                    memmove(inBuffer + inBuffCurrentIndex + 1, inBuffer + inBuffCurrentIndex,
                            inBufIndx - (inBuffCurrentIndex));
                    inBuffer[inBuffCurrentIndex] = inChar;
                    inBuffCurrentIndex++;
                    inBufIndx++;
                    inBuffer[inBufIndx] = 0;

                    int i = inBuffCurrentIndex;
                    for (i = inBuffCurrentIndex - 1; i < inBufIndx; i++) {
                        printf("%c", inBuffer[i]);
                    }

                    for (i = inBuffCurrentIndex; i < inBufIndx; i++) {
                        printf("\b");
                    }

                } else {
                    inBuffer[inBufIndx++] = inChar;
                    inBuffer[inBufIndx] = 0;
                    inBuffCurrentIndex = inBufIndx;
                    printf("%c", inChar);        // echo character
                    break;
                }
            }
        }
    } else {
        // single character mode
        inBufIndx = inBuffCurrentIndex = 0;
        inBuffer[inBufIndx] = 0;
    }
    return;
} // end keyboard_isr


// **********************************************************************
// timer interrupt service routine
//
static void timer_isr() {
    time_t currentTime;                        // current time

    // assert system mode
    assert("timer_isr Error" && superMode);

    // capture current time
    time(&currentTime);

    // timer sampling/signaling code for project 2
    myClkTime = clock();
    if ((myClkTime - myOldClkTime10) >= TEN_SEC) {
        myOldClkTime10 = myOldClkTime10 + TEN_SEC;   // update old
        semSignal(tics10sec);
    }

    // one second timer
    if ((currentTime - oldTime1) >= 1) {
        // signal 1 second
        semSignal(tics1sec);
        oldTime1 += 1;
    }

    // sample fine clock
    myClkTime = clock();
    if ((myClkTime - myOldClkTime) >= ONE_TENTH_SEC) {
        myOldClkTime = myOldClkTime + ONE_TENTH_SEC;   // update old
        semSignal(tics10thsec);
    }
    return;
} // end timer_isr
