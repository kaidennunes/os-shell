#include <stdio.h>
#include <malloc.h>
#include "priorityqueue.h"

priorityQueue createPriorityQueue(int queueSize) {
    priorityQueue queue;
    queue.currentQueueSize = 0;
    queue.maxQueueSize = queueSize;
    queue.queue = malloc(queueSize * sizeof(priorityItem));
    return queue;
}

int moveAllQueueItems(priorityQueue *queueToRemove, priorityQueue *queueToAdd) {
    int i = 0;
    for (i = 0; i < queueToRemove->currentQueueSize; i++) {
        priorityItem queuedItem = queueToRemove->queue[i];
        insertQueueItem(queueToAdd, queuedItem.id, queuedItem.priority);
    }

    queueToRemove->currentQueueSize = 0;
    return 1;
}

int moveQueueItem(priorityQueue *queueToRemove, priorityQueue *queueToAdd, int item) {
    if (queueToRemove->currentQueueSize == 0) {
        return -1;
    }

    priorityItem *newQueue = malloc((*queueToRemove).maxQueueSize * sizeof(priorityItem));
    int priority, i, removedItem = 0;
    for (i = 0; i < queueToRemove->currentQueueSize; i++) {
        priorityItem queuedItem = queueToRemove->queue[i];
        if (queuedItem.id == item && !removedItem) {
            removedItem = 1;
            priority = queuedItem.priority;
        } else {
            newQueue[i - removedItem].id = queueToRemove->queue[i].id;
            newQueue[i - removedItem].priority = queueToRemove->queue[i].priority;
        }
    }

    queueToRemove->currentQueueSize--;
    free(queueToRemove->queue);
    queueToRemove->queue = newQueue;

    insertQueueItem(queueToAdd, item, priority);

    return 0;
}


int getHighestPriority(priorityQueue *queue) {
    if (queue->currentQueueSize == 0) {
        return -1;
    }

    queue->currentQueueSize--;
    return queue->queue[queue->currentQueueSize].id;
}

int insertQueueItem(priorityQueue *queue, int item, int priority) {
    if (queue->maxQueueSize == queue->currentQueueSize) {
        return -1;
    }

    priorityItem *newQueue = malloc(queue->maxQueueSize * sizeof(priorityItem));
    int addedItem = 0;
    int i = 0;
    for (i = 0; i < queue->currentQueueSize; i++) {
        if (queue->queue[i].priority >= priority && !addedItem) {
            newQueue[i].id = item;
            newQueue[i].priority = priority;
            addedItem = 1;
        } else {
            newQueue[i].id = queue->queue[i - addedItem].id;
            newQueue[i].priority = queue->queue[i - addedItem].priority;
        }
    }

    if (addedItem) {
        newQueue[i].id = queue->queue[i - addedItem].id;
        newQueue[i].priority = queue->queue[i - addedItem].priority;
    } else {
        newQueue[i].id = item;
        newQueue[i].priority = priority;
    }

    queue->currentQueueSize++;
    free(queue->queue);
    queue->queue = newQueue;
    return 0;
}

int destroyPriorityQueue(priorityQueue *queue) {
    free(queue->queue);
    return 1;
}