// os345fat.c - file management system	2017-06-28
// ***********************************************************************
// **   DISCLAMER ** DISCLAMER ** DISCLAMER ** DISCLAMER ** DISCLAMER   **
// **                                                                   **
// ** The code given here is the basis for the CS345 projects.          **
// ** It comes "as is" and "unwarranted."  As such, when you use part   **
// ** or all of the code, it becomes "yours" and you are responsible to **
// ** understand any algorithm or method presented.  Likewise, any      **
// ** errors or problems become your responsibility to fix.             **
// **                                                                   **
// ** NOTES:                                                            **
// ** -Comments beginning with "// ??" may require some implementation. **
// ** -Tab stops are set at every 3 spaces.                             **
// ** -The function API's in "OS345.h" should not be altered.           **
// **                                                                   **
// **   DISCLAMER ** DISCLAMER ** DISCLAMER ** DISCLAMER ** DISCLAMER   **
// ***********************************************************************
//
//		11/19/2011	moved getNextDirEntry to P6
//
// ***********************************************************************
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <setjmp.h>
#include <assert.h>
#include "os345.h"
#include "os345fat.h"

// ***********************************************************************
// ***********************************************************************
//	functions to implement in Project 6
//
int fmsCloseFile(int);

int fmsDefineFile(char *, int);

int fmsDeleteFile(char *);

int fmsOpenFile(char *, int);

int fmsReadFile(int, char *, int);

int fmsSeekFile(int, int);

int fmsWriteFile(int, char *, int);

// ***********************************************************************
// ***********************************************************************
//	custom helper functions
//

int findAvailableCluster();

// ***********************************************************************
// ***********************************************************************
//	Support functions available in os345p6.c
//
extern int fmsGetDirEntry(char *fileName, DirEntry *dirEntry);

extern int fmsGetNextDirEntry(int *dirNum, char *mask, DirEntry *dirEntry, int dir);

extern int fmsMount(char *fileName, void *ramDisk);

extern void setFatEntry(int FATindex, unsigned short FAT12ClusEntryVal, unsigned char *FAT);

extern unsigned short getFatEntry(int FATindex, unsigned char *FATtable);

extern int fmsMask(char *mask, char *name, char *ext);

extern void setDirTimeDate(DirEntry *dir);

extern int isValidFileName(char *fileName);

extern void printDirectoryEntry(DirEntry *);

extern void fmsError(int);

extern int fmsReadSector(void *buffer, int sectorNumber);

extern int fmsWriteSector(void *buffer, int sectorNumber);

// ***********************************************************************
// ***********************************************************************
// fms variables
//
// RAM disk
unsigned char RAMDisk[SECTORS_PER_DISK * BYTES_PER_SECTOR];

// File Allocation Tables (FAT1 & FAT2)
unsigned char FAT1[NUM_FAT_SECTORS * BYTES_PER_SECTOR];
unsigned char FAT2[NUM_FAT_SECTORS * BYTES_PER_SECTOR];

char dirPath[128];                            // current directory path
FCB OFTable[NFILES];                        // open file table

extern bool diskMounted;                    // disk has been mounted
extern TCB tcb[];                            // task control block
extern int curTask;                            // current task #


// ***********************************************************************
// ***********************************************************************
// This function closes the open file specified by fileDescriptor.
// The fileDescriptor was returned by fmsOpenFile and is an index into the open file table.
//	Return 0 for success, otherwise, return the error number.
//
int fmsCloseFile(int fileDescriptor) {
    FCB *fileControlBlock;

    // If the file descriptor isn't valid, return an error
    if (fileDescriptor < 0 || fileDescriptor >= NFILES) {
        return ERR52;
    }

    fileControlBlock = &OFTable[fileDescriptor];

    // If the file isn't open, return an error
    if (!fileControlBlock->name[0]) {
        return ERR63;
    }

    // If the file is locked, then return an error
    if (fileControlBlock->flags & FILE_LOCKED) {
        return ERR85;
    }

    // Flush transaction buffer
    int error;
    if (fileControlBlock->flags & BUFFER_ALTERED) {
        //      printf("\nWriting to cluster: %d", fileControlBlock->currentCluster);
        if ((error = fmsWriteSector(fileControlBlock->buffer, C_2_S(fileControlBlock->currentCluster)))) {
            return error;
        }
        fileControlBlock->flags &= ~BUFFER_ALTERED;
    }

    // If the file has changed, then modify the directory entry
    if (fileControlBlock->flags & FILE_ALTERED) {
        // Get file name into buffer
        char fileName[FILE_NAME_LENGTH + FILE_EXTENSION_LENGTH + 2];

        int i;
        for (i = 0; i < FILE_NAME_LENGTH; i++) {
            if (fileControlBlock->name[i] == 0) {
                break;
            }
            fileName[i] = fileControlBlock->name[i];
        }

        fileName[i] = '.';

        int j;
        for (j = 0; j < FILE_EXTENSION_LENGTH; j++) {
            if (fileControlBlock->extension[j] == 0) {
                break;
            }
            fileName[i + j + 1] = fileControlBlock->extension[j];
        }

        fileName[i + j + 1] = '\0';

        // Update the directory;
        DirEntry dirEntryToSave;
        int value = 0;
        int error = fmsGetNextDirEntry(&value, fileName, &dirEntryToSave, fileControlBlock->directoryCluster);
        if (error) {
            return UNDEFINED;
        }

        // Update end of file and creation date/time
        dirEntryToSave.fileSize = fileControlBlock->fileSize;
        dirEntryToSave.startCluster = fileControlBlock->startCluster;
        setDirTimeDate(&dirEntryToSave);

        int offset = value / ENTRIES_PER_SECTOR;
        int index = (value % ENTRIES_PER_SECTOR) - 1;
        if (index == -1) {
            offset--;
            index = ENTRIES_PER_SECTOR - 1;
        }
        DirEntry *buff = calloc(1, BYTES_PER_SECTOR);
        if (CDIR == 0) {
            int sector = BEG_ROOT_SECTOR + offset;
            fmsReadSector(buff, sector);
            buff[index] = dirEntryToSave;
            fmsWriteSector(buff, sector);
        } else {
            int iter = CDIR;
            for (i = 0; i < offset; i++) {
                iter = getFatEntry(iter, FAT1);
            }
            fmsReadSector(buff, C_2_S(iter));
            buff[index] = dirEntryToSave;
            fmsWriteSector(buff, C_2_S(iter));
        }
    }

    // Close the file
    int i;
    for (i = 0; i < FILE_NAME_LENGTH; i++) {
        fileControlBlock->name[i] = 0;
    }
    for (i = 0; i < FILE_EXTENSION_LENGTH; i++) {
        fileControlBlock->extension[i] = 0;
    }
    fileControlBlock->attributes = 0;
    fileControlBlock->directoryCluster = 0;
    fileControlBlock->startCluster = 0;
    fileControlBlock->currentCluster = 0;
    fileControlBlock->fileSize = 0;
    fileControlBlock->pid = 0;
    fileControlBlock->mode = 0;
    fileControlBlock->flags = 0;
    fileControlBlock->fileIndex = 0;

    // Return success
    return 0;
} // end fmsCloseFile



// ***********************************************************************
// ***********************************************************************
// If attribute=DIRECTORY, this function creates a new directory
// file directoryName in the current directory.
// The directory entries "." and ".." are also defined.
// It is an error to try and create a directory that already exists.
//
// else, this function creates a new file fileName in the current directory.
// It is an error to try and create a file that already exists.
// The start cluster field should be initialized to cluster 0.  In FAT-12,
// files of size 0 should point to cluster 0 (otherwise chkdsk should report an error).
// Remember to change the start cluster field from 0 to a free cluster when writing to the
// file.
//
// Return 0 for success, otherwise, return the error number.
//
int fmsDefineFile(char *fileName, int attribute) {
    // Check to make sure the file name is valid
    if (isValidFileName(fileName) != 1) {
        return ERR50;
    }

    DirEntry directory;
    int error = fmsGetDirEntry(fileName, &directory);
    if (error == 0) {
        return ERR60;
    }

    char *tempstr = calloc(strlen(fileName) + 1, sizeof(char));
    strcpy(tempstr, fileName);
    char *search = ".";
    char *fileNamePart = strtok(tempstr, search);
    char *fileExtensionPart = strtok(NULL, search);


    size_t fileNameSize = strlen(fileNamePart);
    size_t fileExtensionSize = fileExtensionPart == NULL ? 0 : strlen(fileExtensionPart);

    DirEntry newEntry;
    int i;
    for (i = 0; i < fileNameSize; i++) {
        newEntry.name[i] = toupper(fileNamePart[i]);
    }

    for (; i < FILE_NAME_LENGTH; i++) {
        newEntry.name[i] = ' ';
    }

    for (i = 0; i < fileExtensionSize; i++) {
        newEntry.extension[i] = toupper(fileExtensionPart[i]);
    }

    for (; i < FILE_EXTENSION_LENGTH; i++) {
        newEntry.extension[i] = ' ';
    }

    newEntry.fileSize = 0;
    newEntry.attributes = attribute;
    setDirTimeDate(&newEntry);


    int value = 2;
    DirEntry dirEntry;
    error = fmsGetNextDirEntry(&value, "", &dirEntry, CDIR);

    int offset = value / ENTRIES_PER_SECTOR;
    int index = value % ENTRIES_PER_SECTOR;

/*    int offset = value / ENTRIES_PER_SECTOR;
    int index = (value % ENTRIES_PER_SECTOR) - 1;
    if (index == -1) {
        offset--;
        index = ENTRIES_PER_SECTOR - 1;
    }*/

    if (attribute & DIRECTORY) {
        int availableCluster = findAvailableCluster();
        if (availableCluster == -1) {
            return ERR64;
        }

        if (getFatEntry(availableCluster, FAT1) == 0) {
            setFatEntry(availableCluster, FAT_EOC, FAT1);
            setFatEntry(availableCluster, FAT_EOC, FAT2);
            newEntry.startCluster = availableCluster;
            DirEntry *buff = calloc(1, BYTES_PER_SECTOR);

            DirEntry dir1;
            dir1.fileSize = 0;
            setDirTimeDate(&dir1);
            dir1.attributes = DIRECTORY;
            dir1.name[0] = '.';

            int j;
            for (j = 1; j < FILE_NAME_LENGTH; j++) {
                dir1.name[j] = ' ';
            }

            for (j = 0; j < FILE_EXTENSION_LENGTH; j++) {
                dir1.extension[j] = ' ';
            }
            dir1.startCluster = i;

            DirEntry dir2;
            dir2.fileSize = 0;
            setDirTimeDate(&dir2);
            dir2.attributes = DIRECTORY;
            dir2.name[0] = '.';
            dir2.name[1] = '.';

            for (j = 2; j < FILE_NAME_LENGTH; j++) {
                dir2.name[j] = ' ';
            }
            for (j = 0; j < FILE_EXTENSION_LENGTH; j++) {
                dir2.extension[j] = ' ';
            }

            dir2.startCluster = CDIR;

            buff[0] = dir1;
            buff[1] = dir2;
            fmsWriteSector(buff, C_2_S(availableCluster));
        }
    } else {
        newEntry.startCluster = 0;
    }

    DirEntry *buff = calloc(1, BYTES_PER_SECTOR);
    if (CDIR == 0) {
        int sector = BEG_ROOT_SECTOR + offset;
        fmsReadSector(buff, sector);
        buff[index] = newEntry;
        fmsWriteSector(buff, sector);
    } else {
        int iter = CDIR;
        for (i = 0; i < index; i++) {
            iter = getFatEntry(iter, FAT1);
        }
        fmsReadSector(buff, C_2_S(iter));
        buff[index] = newEntry;
        fmsWriteSector(buff, C_2_S(iter));
    }
    return 0;
} // end fmsDefineFile



// ***********************************************************************
// ***********************************************************************
// This function deletes the file fileName from the current director.
// The file name should be marked with an "E5" as the first character and the chained
// clusters in FAT 1 reallocated (cleared to 0).
// Return 0 for success; otherwise, return the error number.
//
int fmsDeleteFile(char *fileName) {
    // Check to make sure the file name is valid
    if (isValidFileName(fileName) != 1) {
        return ERR50;
    }

    DirEntry dir, directoryToDelete;
    int value = 0;
    int error = fmsGetNextDirEntry(&value, fileName, &directoryToDelete, CDIR);
    if (error) {
        return ERR61;
    }


    int offset = (value / ENTRIES_PER_SECTOR);
    int index = (value % ENTRIES_PER_SECTOR) - 1;
    if (index == -1) {
        offset--;
        index = ENTRIES_PER_SECTOR - 1;
    }

    if (directoryToDelete.attributes & DIRECTORY) {
        int fileValue = 2;
        error = fmsGetNextDirEntry(&fileValue, "*.*", &dir, directoryToDelete.startCluster);
        if (!error) {
            return ERR69;
        }
    }

    directoryToDelete.name[0] = 0xe5;

    int currentCluster = directoryToDelete.startCluster;
    while (TRUE) {
        int nextCluster = getFatEntry(currentCluster, FAT1);
        if (nextCluster == FAT_EOC || nextCluster == 0) {
            break;
        }
        setFatEntry(currentCluster, 0, FAT1);
        currentCluster = nextCluster;
    }

    // Save the deletion
    DirEntry *buff = calloc(1, BYTES_PER_SECTOR);
    if (CDIR == 0) {
        fmsReadSector(buff, BEG_ROOT_SECTOR + offset);
        buff[index] = directoryToDelete;
        fmsWriteSector(buff, BEG_ROOT_SECTOR + offset);
    } else {
        int iter = CDIR;
        for (int i = 0; i < offset; i++) {
            iter = getFatEntry(iter, FAT1);
        }
        fmsReadSector(buff, C_2_S(iter));
        buff[index] = directoryToDelete;
        fmsWriteSector(buff, C_2_S(iter));
    }

    return 0;
} // end fmsDeleteFile

// ***********************************************************************
// ***********************************************************************
// This function opens the file fileName for access as specified by rwMode.
// It is an error to try to open a file that does not exist.
// The open mode rwMode is defined as follows:
//    0 - Read access only.
//       The file pointer is initialized to the beginning of the file.
//       Writing to this file is not allowed.
//    1 - Write access only.
//       The file pointer is initialized to the beginning of the file.
//       Reading from this file is not allowed.
//    2 - Append access.
//       The file pointer is moved to the end of the file.
//       Reading from this file is not allowed.
//    3 - Read/Write access.
//       The file pointer is initialized to the beginning of the file.
//       Both read and writing to the file is allowed.
// A maximum of 32 files may be open at any one time.
// If successful, return a file descriptor that is used in calling subsequent file
// handling functions; otherwise, return the error number.
//
int fmsOpenFile(char *fileName, int rwMode) {
    // Check to make sure the file name is valid
    if (isValidFileName(fileName) != 1) {
        return ERR50;
    }

    // Look for the directory entry
    DirEntry dirEntry;
    int error = fmsGetDirEntry(fileName, &dirEntry);

    // See if the file actually exists
    if (error) {
        return ERR61;
    }



    // Split up the name into file name and extension
    char *tempstr = calloc(strlen(fileName) + 1, sizeof(char));
    strcpy(tempstr, fileName);
    char *search = ".";
    char *fileNamePart = strtok(tempstr, search);
    char *fileExtensionPart = strtok(NULL, search);

    size_t fileNameSize = strlen(fileNamePart);
    size_t fileExtensionSize = strlen(fileExtensionPart);

    // Find an open slot in the open file table
    unsigned int i;
    int fileDescriptor = -1;
    for (i = 0; i < NFILES; i++) {
        // Use the first open slot we find
        if (!OFTable[i].name[0] && fileDescriptor < 0) {
            fileDescriptor = i;
            continue;
        } else if (OFTable[i].name[0]) {
            // Compare the file names to make sure they aren't the same
            bool sameFile = TRUE;
            unsigned int j;
            for (j = 0; j < fileNameSize; j++) {
                if (toupper(fileNamePart[j]) != OFTable[i].name[j]) {
                    sameFile = FALSE;
                    break;
                }
            }
            // If the first fileNameSize elements are the same, if the open file has more to its name, they aren't the same file
            for (; j < FILE_NAME_LENGTH && sameFile; j++) {
                if (OFTable[i].name[j]) {
                    sameFile = FALSE;
                    break;
                }
            }

            // Now check the file extension
            for (j = 0; j < fileExtensionSize && sameFile; j++) {
                if (toupper(fileExtensionPart[j]) != OFTable[i].extension[j]) {
                    sameFile = FALSE;
                    break;
                }
            }
            // If the first fileNameSize elements are the same, if the open file has more to its name, they aren't the same file
            for (; j < FILE_EXTENSION_LENGTH && sameFile; j++) {
                if (OFTable[i].extension[j]) {
                    sameFile = FALSE;
                    break;
                }
            }

            if (sameFile) {
                return ERR62;
            }
        }
    }

    // If we didn't find an open slot, then return an error
    if (fileDescriptor < 0) {
        return ERR70;
    }

    // Open the file. Start by putting in the file name
    for (i = 0; i < fileNameSize; i++) {
        OFTable[fileDescriptor].name[i] = toupper(fileNamePart[i]);
    }

    for (i = 0; i < fileExtensionSize; i++) {
        OFTable[fileDescriptor].extension[i] = toupper(fileExtensionPart[i]);
    }

    OFTable[fileDescriptor].attributes = dirEntry.attributes;
    OFTable[fileDescriptor].directoryCluster = CDIR;
    OFTable[fileDescriptor].startCluster = dirEntry.startCluster;
    OFTable[fileDescriptor].currentCluster = 0;
    OFTable[fileDescriptor].fileSize = (rwMode == OPEN_WRITE) ? 0 : dirEntry.fileSize;
    OFTable[fileDescriptor].pid = curTask;
    OFTable[fileDescriptor].mode = rwMode;
    OFTable[fileDescriptor].flags = 0;
    OFTable[fileDescriptor].fileIndex = (rwMode != OPEN_APPEND) ? 0 : dirEntry.fileSize;
    return fileDescriptor;
} // end fmsOpenFile



// ***********************************************************************
// ***********************************************************************
// This function reads nBytes bytes from the open file specified by fileDescriptor into
// memory pointed to by buffer.
// The fileDescriptor was returned by fmsOpenFile and is an index into the open file table.
// After each read, the file pointer is advanced.
// Return the number of bytes successfully read (if > 0) or return an error number.
// (If you are already at the end of the file, return EOF error.  ie. you should never
// return a 0.)
//
int fmsReadFile(int fileDescriptor, char *buffer, int nBytes) {
    int error, nextCluster;
    FCB *fileControlBlock;
    int numBytesRead = 0;
    unsigned int bytesLeft, bufferIndex;
    if (fileDescriptor < 0 || fileDescriptor >= NFILES) {
        return ERR52;
    }
    fileControlBlock = &OFTable[fileDescriptor];
    if (!fileControlBlock->name[0]) {
        return ERR63;
    }
    if (fileControlBlock->mode == OPEN_WRITE || fileControlBlock->mode == OPEN_APPEND) {
        return ERR85;
    }
    while (nBytes > 0) {
        if (fileControlBlock->fileSize == fileControlBlock->fileIndex) {
            return (numBytesRead ? numBytesRead : ERR66);
        }
        bufferIndex = fileControlBlock->fileIndex % BYTES_PER_SECTOR;
        if ((bufferIndex == 0) && (fileControlBlock->fileIndex || !fileControlBlock->currentCluster)) {
            if (!fileControlBlock->currentCluster) {
                if (!fileControlBlock->startCluster) {
                    return ERR66;
                }
                nextCluster = fileControlBlock->startCluster;
                fileControlBlock->fileIndex = 0;
            } else {
                nextCluster = getFatEntry(fileControlBlock->currentCluster, FAT1);
                if (nextCluster == FAT_EOC) {
                    return numBytesRead;
                }
            }
            if (fileControlBlock->flags & BUFFER_ALTERED) {
                printf("\nWrote to sector %d", C_2_S(fileControlBlock->currentCluster));
                if ((error = fmsWriteSector(fileControlBlock->buffer, C_2_S(fileControlBlock->currentCluster)))) {
                    return error;
                }
                fileControlBlock->flags &= ~BUFFER_ALTERED;
            }
            if ((error = fmsReadSector(fileControlBlock->buffer, C_2_S(nextCluster)))) {
                return error;
            }
            fileControlBlock->currentCluster = nextCluster;
        }
        bytesLeft = BYTES_PER_SECTOR - bufferIndex;
        if (bytesLeft > nBytes) {
            bytesLeft = nBytes;
        }
        if (bytesLeft > (fileControlBlock->fileSize - fileControlBlock->fileIndex)) {
            bytesLeft = fileControlBlock->fileSize - fileControlBlock->fileIndex;
        }
        memcpy(buffer, &fileControlBlock->buffer[bufferIndex], bytesLeft);
        fileControlBlock->fileIndex += bytesLeft;
        numBytesRead += bytesLeft;
        buffer += bytesLeft;
        nBytes -= bytesLeft;
    }
    return numBytesRead;
} // end fmsReadFile



// ***********************************************************************
// ***********************************************************************
// This function changes the current file pointer of the open file specified by
// fileDescriptor to the new file position specified by index.
// The fileDescriptor was returned by fmsOpenFile and is an index into the open file table.
// The file position may not be positioned beyond the end of the file.
// Return the new position in the file if successful; otherwise, return the error number.
//
int fmsSeekFile(int fileDescriptor, int index) {
    FCB *fileControlBlock;

    // If the file descriptor isn't valid, return an error
    if (fileDescriptor < 0 || fileDescriptor >= NFILES) {
        return ERR52;
    }

    fileControlBlock = &OFTable[fileDescriptor];

    // If the file isn't open, return an error
    if (!fileControlBlock->name[0]) {
        return ERR63;
    }

    if (fileControlBlock->mode != OPEN_READ && fileControlBlock->mode != OPEN_RDWR) {
        return ERR85;
    }

    if (fileControlBlock->fileSize <= index) {
        return ERR80;
    }

    fileControlBlock->fileIndex = index;

/*
    int length = index / BYTES_PER_SECTOR;
    fileControlBlock->currentCluster = fileControlBlock->startCluster;

    for (int i = 0; i < length; i++) {
        fileControlBlock->currentCluster = getFatEntry(fileControlBlock->currentCluster, FAT1);
    }

    if (length == 0) {
        fileControlBlock->currentCluster = fileControlBlock->startCluster;
    }*/

    return index;
} // end fmsSeekFile



// ***********************************************************************
// ***********************************************************************
// This function writes nBytes bytes to the open file specified by fileDescriptor from
// memory pointed to by buffer.
// The fileDescriptor was returned by fmsOpenFile and is an index into the open file table.
// Writing is always "overwriting" not "inserting" in the file and always writes forward
// from the current file pointer position.
// Return the number of bytes successfully written; otherwise, return the error number.
//
int fmsWriteFile(int fileDescriptor, char *buffer, int nBytes) {
    int error, nextCluster;
    FCB *fileControlBlock;
    int numBytesWritten = 0;
    unsigned int bytesLeft, bufferIndex;
    if (fileDescriptor < 0 || fileDescriptor >= NFILES) {
        return ERR52;
    }
    fileControlBlock = &OFTable[fileDescriptor];

    if (!fileControlBlock->name[0]) {
        return ERR63;
    }
    if (fileControlBlock->mode == OPEN_READ) {
        return ERR84;
    }

    // If we are overwriting from the beginning of the file, then clear out the cluster chain
    /*if (fileControlBlock->fileSize == 0) {
        int currentCluster = fileControlBlock->startCluster;
        while (TRUE) {
            int nextCluster = getFatEntry(currentCluster, FAT1);
            if (nextCluster == FAT_EOC || nextCluster == 0) {
                break;
            }
            setFatEntry(currentCluster, 0, FAT1);
            currentCluster = nextCluster;
        }
    }*/

    if (fileControlBlock->startCluster == 0) {
        int availableCluster = findAvailableCluster();
        fileControlBlock->startCluster = availableCluster;
        fileControlBlock->currentCluster = availableCluster;
        // printf("\nFound available cluster: %d", availableCluster);
        setFatEntry(availableCluster, FAT_EOC, FAT1);
        setFatEntry(availableCluster, FAT_EOC, FAT2);
    } else if (fileControlBlock->mode == OPEN_APPEND) {
        // printf("\nFinding current cluster");
        int length = fileControlBlock->fileIndex / BYTES_PER_SECTOR;
        // printf("\nLength: %d", length);
        fileControlBlock->currentCluster = fileControlBlock->startCluster;
        for (int i = 0; i < length; i++) {
            fileControlBlock->currentCluster = getFatEntry(fileControlBlock->currentCluster, FAT1);
        }
        if (length == 0) {
            fileControlBlock->currentCluster = fileControlBlock->startCluster;
        }
        //   printf("\nCurrent cluster: %d", fileControlBlock->currentCluster);
    }

    while (nBytes > 0) {
        bufferIndex = fileControlBlock->fileIndex % BYTES_PER_SECTOR;
        if ((bufferIndex == 0) && (fileControlBlock->fileIndex || fileControlBlock->currentCluster == 0)) {
            //       printf("\nCurrent buffer index: %d", bufferIndex);
            //      printf("\nCurrent file index: %d", fileControlBlock->fileIndex);
            //     printf("\nCurrent cluster : %d", fileControlBlock->currentCluster);

            if (fileControlBlock->currentCluster == 0) {
                // If the file is newly created, then start at the beginning cluster
                if (fileControlBlock->startCluster == 0) {
                    return ERR66;
                }
                //        printf("\nStarting over");
                nextCluster = fileControlBlock->startCluster;
                fileControlBlock->fileIndex = 0;
            } else {
                //           printf("\nGetting next cluster from cluster%d: ", fileControlBlock->currentCluster);
                nextCluster = getFatEntry(fileControlBlock->currentCluster, FAT1);
                if (nextCluster == FAT_EOC) {
                    //             printf("\nFinding an available cluster");
                    int availableCluster = findAvailableCluster();
                    if (availableCluster == -1) {
                        return ERR65;
                    }

                    setFatEntry(fileControlBlock->currentCluster, availableCluster, FAT1);
                    setFatEntry(availableCluster, FAT_EOC, FAT1);

                    setFatEntry(fileControlBlock->currentCluster, availableCluster, FAT2);
                    setFatEntry(availableCluster, FAT_EOC, FAT2);

                    nextCluster = getFatEntry(fileControlBlock->currentCluster, FAT1);
                }
                //              printf("\nNext cluster: %d: ", nextCluster);
            }

            //         printf("\nOutside loop: Current buffer index: %d", bufferIndex);
            //        printf("\nOutside loop: Current file index: %d", fileControlBlock->fileIndex);
            //       printf("\nOutside loop: Current cluster : %d", fileControlBlock->currentCluster);

            if (fileControlBlock->flags & BUFFER_ALTERED) {
                //               printf("\nCopying buffer to cluster within loop: %d: ", fileControlBlock->currentCluster);
                if ((error = fmsWriteSector(fileControlBlock->buffer, C_2_S(fileControlBlock->currentCluster)))) {
                    return error;
                }
                fileControlBlock->flags &= ~BUFFER_ALTERED;
            }

            //          printf("\nCurrent cluster is %d, switching to next cluster at %d: ", fileControlBlock->currentCluster,
            //                nextCluster);

            fileControlBlock->currentCluster = nextCluster;
        }

//        printf("\nOutside everything: Current buffer index: %d", bufferIndex);
        //       printf("\nOutside everything: Current file index: %d", fileControlBlock->fileIndex);
        //      printf("\nOutside everything: Current cluster : %d", fileControlBlock->currentCluster);

        bytesLeft = BYTES_PER_SECTOR - bufferIndex;
        if (bytesLeft > nBytes) {
            bytesLeft = nBytes;
        }

        memcpy(&fileControlBlock->buffer[bufferIndex], buffer, bytesLeft);
        fileControlBlock->fileIndex += bytesLeft;
        numBytesWritten += bytesLeft;
        buffer += bytesLeft;
        nBytes -= bytesLeft;
        fileControlBlock->flags |= BUFFER_ALTERED;
        /*if (fileControlBlock->flags & BUFFER_ALTERED || TRUE) {
                printf("\nCopying buffer to cluster: %d: ", fileControlBlock->currentCluster);
            if ((error = fmsWriteSector(fileControlBlock->buffer, C_2_S(fileControlBlock->currentCluster)))) {
                return error;
            }
            fileControlBlock->flags &= ~BUFFER_ALTERED;
        }*/
    }
    fileControlBlock->fileSize = fileControlBlock->fileSize + numBytesWritten;
    fileControlBlock->flags |= FILE_ALTERED;
    return numBytesWritten;
} // end fmsWriteFile

int findAvailableCluster() {
    for (int i = 2; i < NUM_FAT_SECTORS * BYTES_PER_SECTOR; i++) {
        if (getFatEntry(i, FAT1) == 0) {
            return i;
        }
    }
    return -1;
}
